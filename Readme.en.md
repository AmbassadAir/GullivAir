Ambassad'Air
===

## Presentation

Ambassad'Air is a projet which aims at mobilizing citizens on air quality.

The project is composed of different modules. Each module has its specification, but all must be easily rebuilt.

## Documentation

You can find the documentation of this project on the [Wiki](https://gitlab.com/AmbassadAir/AirBeam/wikis/home).

### Contributions

Ambassad'Air is an open source project, and we would be very glad to accept any contributions from the community. Please refer to [Contributions](https://gitlab.com/AmbassadAir/AirBeam/wikis/contributions) for more information.

## Licence

This project is under the GPLv3 license. For more information, please refer to [https://www.gnu.org/licenses/gpl-3.0.html](https://www.gnu.org/licenses/gpl-3.0.html).

## Project structure

 - Probe: code on the Arduino to collect sensors data, and transmit them to other devices
 - WifiViewer: code on the ESP8266 to transmit the data (collected by the probes) to Wifi devices like smartphones or computers

## Requirements

### Arduino

 - [ArduinoJson](https://github.com/bblanchon/ArduinoJson)
 - [ESP8266](https://github.com/esp8266/Arduino/)

## Help

[ESP8266 WiFi examples](https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi/examples)

## Development

WifiViewer/test-server.js enables to test the WifiViewer/index.html client page