Ambassad'Air
===

## Présentation du projet

Ambassad'Air est un projet qui vise à mobiliser les habitants sur la qualité de l'air à Rennes.

Le projet se décompose en plusieurs réalisations de boîtiers de mesures.

Chaque boîtier a ses propres caractéristiques techniques, mais tous ont vocation à être reproduit librement.


## Documentation du projet

Vous pouvez trouver la documentation de ce projet dans le [Wiki](https://gitlab.com/AmbassadAir/AirBeam/wikis/home).

### Contributions

Ambassad'Air est un projet open source et nous sommes très heureux d'accepter les contributions de la communauté. Veuillez vous référer à la page [Contributions](https://gitlab.com/AmbassadAir/AirBeam/wikis/contributions) pour plus de détails.

## Licence

Le projet est sous licence GPLv3. Pour plus d'informations, regarder les droits en visitant cette page [https://www.gnu.org/licenses/gpl-3.0.html](https://www.gnu.org/licenses/gpl-3.0.html).

## Structure du projet

 - Probe: logiciel sur l'Arduino qui permet de collecter les données des capteurs, puis de les transmettre aux autres appareils (comme l'ESP8266)
 - WifiViewer: logiciel sur le ESP8266 qui permet de transmettre les données des capteurs (reçues depuis l'Arduino) vers un appareil Wifi (smartphone ou ordinateur)

## Bibliothèques

### Arduino

 - [ArduinoJson](https://github.com/bblanchon/ArduinoJson)
 - [ESP8266](https://github.com/esp8266/Arduino/)

## Aide

[Examples ESP8266 WiFi](https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi/examples)

## Développement

WifiViewer/test-server.js permet de tester la page WifiViewer/index.html
