/*
 * Copyleft - VinCiv pour Gulliver - 2017
 *
 * Gnu GPL3 License
 * Code placé sous licence Gnu GPL3
 */

// WifiServer is an access point which serves a simple html webpage to see the sensors data in real time
// An arduino sends sensors data through the custom serial port, the ESP8266 reads data and sends it to the connected web client (smartphone, computers)

// WifiServer est un point d'accès Wifi qui fournit une page web pour visualiser les données collectées par les capteurs
// Une carte Arduino envoie les données collectées par un port série logiciel, l'ESP8266 lit les données et les envoie aux client connectés (smartphone, ordinateurs)

// Todo:
// - save the html to serve on a file on the ESP memory?

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
#include "FS.h"
#include <SoftwareSerial.h>

const char *ssid = "GullivAir";
const char *password = "gulliver";

ESP8266WebServer server(80);
char gMessage[256];
static int count = 0;
SoftwareSerial softwareSerial(8, 9); // RX, TX

// Serves the main html file for the client interface (accessed from a smartphone for example)
// Todo: save the .html file in memory instead of hardcoding it?
// Fournis le fichier html pour l'interface client (accessible depuis un smartphone)
void handleRoot() {
  // File f = SPIFFS.open("/index-min.html", "r");
	// size_t size = f.size();
	// char buffer[size];
	// f.readBytes(buffer, size);
	// server.send(200, "text/html", buffer);
	// server.send(200, "text/html", f.readString());
	server.send(200, "text/html", "<!DOCTYPE html> <html> <head> <style>body{ }</style> <script>var xmlhttp = new XMLHttpRequest(); var url = 'data/'; xmlhttp.onreadystatechange = function() { if (this.readyState == 4 && this.status == 200) { var data = JSON.parse(this.responseText); displayData(data); } }; function displayData(data) { document.getElementById('Message', data.message); document.getElementById('Temperature', data.temperature); document.getElementById('N02', data.N02); document.getElementById('PM', data.PM); }; getData() { xmlhttp.open('GET', url, true); xmlhttp.send(); }; document.addEventListener('DOMContentLoaded', function(event) { setInterval(getData, 5000); });</script> </head> <body> <ul> <li>Message: <span id='Message'>--</span></li> <li>N20: <span id='N02'>--</span></li> <li>Particules: <span id='PM'>--</span></li> <li>Temperature: <span id='Temperature'>--</span></li> </ul> </body> </html>");
}

// response to sensor data request (asked by the client javascript)
void handleDataRequest() {

	// Create a JSON object form the data
	StaticJsonBuffer<200> jsonBuffer;
	JsonObject& root = jsonBuffer.createObject();
	root["message"] = gMessage;
	root["count"] = ++count;
	root["latitude"] = 52.165000; //TODO
	root["longitude"] = -2.210000;
	root["temperature"] = 1;
	root["NO2"] = 2;
	root["PM"] = 3;

	size_t len = root.measureLength();
	size_t size = len+1;
	char json[size];
	root.printTo(json, sizeof(json));

	server.setContentLength (size);
	// server.sendHeader (const String &name, const String &value, bool first=false)
	// server.sendContent (json);
	server.send(200, "application/json", json);
}

// Start wifi hotshopt
void setup() {

	delay(1000);
	Serial.begin(115200);

	IPAddress ESP_IP = WiFi.softAP(ssid, password);

	server.on("/", handleRoot);
	server.on("/data", handleDataRequest);

	server.begin();
}

// Read data from software serial port (the connected arduino regularly sends sensor data)
// Serve html page
void loop() {
	
	// Read message from USB serial port for debug purpose
	int length = 256;
	char buffer[256];
	char character ='\n';
	length = Serial.readBytesUntil(character, buffer, length);
	if ( length > 0) {
		memcpy( gMessage, buffer, length);
		gMessage[length] = 0;
	}

	// Serve html page
	server.handleClient();

	delay(500);

	// Read data from software serial port (the connected arduino regularly sends sensor data)
	// Lit les données sur le port série logiciel (l'arduino envoie régulièrement des données)
	if (softwareSerial.available())
	{
		gMessage = softwareSerial.read();
		// Serial.write(softwareSerial.read());
	}
}
